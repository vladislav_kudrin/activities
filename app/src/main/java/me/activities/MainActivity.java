package me.activities;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;
import android.util.Log;
import android.widget.TextView;

/**
 * Runs an application's activity.
 *
 * @author Vladislav
 * @version 1.1
 * @since 03/04/2021
 */
public class MainActivity extends AppCompatActivity {
    private static final String TAG = "Started activity";
    private int five = 5;

    /**
     * Creates the application's activity.
     *
     * @param savedInstanceState previous saved data states.
     */
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toast.makeText(this, "onCreate", Toast.LENGTH_SHORT).show();
        Log.d(TAG, "onCreate");
    }

    /**
     * Restarts the application's activity.
     */
    public void onRestart(){
        super.onRestart();
        Toast.makeText(this, "onRestart", Toast.LENGTH_SHORT).show();
        Log.d(TAG, "onRestart");
    }

    /**
     * Makes the application's activity visible.
     */
    protected void onStart() {
        super.onStart();
        Toast.makeText(this, "onStart", Toast.LENGTH_SHORT).show();
        Log.d(TAG, "onStart");
        resetUI();
    }

    /**
     * Restores data states from {@code savedInstanceState}.
     *
     * @param savedInstanceState data states for restoring.
     */
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        five = savedInstanceState.getInt("FIVE_STATE_KEY");
        Toast.makeText(this, "onRestoreInstanceState", Toast.LENGTH_SHORT).show();
        Log.d(TAG, "onRestoreInstanceState");
    }

    /**
     * Makes the application's activity visible when the activity will start interacting with the user.
     */
    public void onResume(){
        super.onResume();
        Toast.makeText(this, "onResume", Toast.LENGTH_SHORT).show();
        Log.d(TAG, "onResume");
    }

    /**
     * Saves data states to {@code outState}.
     *
     * @param outState data states for saving.
     */
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt("FIVE_STATE_KEY", five);
        Toast.makeText(this, "onSaveInstanceState", Toast.LENGTH_SHORT).show();
        Log.d(TAG, "onSaveInstanceState");
        super.onSaveInstanceState(outState);
    }

    /**
     * Makes the application's activity inactive.
     */
    public void onPause() {
        super.onPause();
        Toast.makeText(this, "onPause", Toast.LENGTH_SHORT).show();
        Log.d(TAG, "onPause");
    }

    /**
     * Stops the application's activity.
     */
    public void onStop() {
        super.onStop();
        Toast.makeText(this, "onStop", Toast.LENGTH_SHORT).show();
        Log.d(TAG, "onStop");
    }

    /**
     * Destroys the application's activity.
     */
    public void onDestroy(){
        super.onDestroy();
        Toast.makeText(this, "onDestroy", Toast.LENGTH_SHORT).show();
        Log.d(TAG, "onDestroy");
    }

    /**
     * Changes text of {@code TextView} to {@code five}.
     */
    private void resetUI() {
        ((TextView) findViewById(R.id.text)).setText(String.valueOf(five));
        Toast.makeText(this, "resetUI", Toast.LENGTH_SHORT).show();
        Log.d(TAG, "resetUI");
    }
}